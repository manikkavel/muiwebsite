import './App.css';
import ListItems from './components/list/sharedWtihMe';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import React, { lazy, Suspense } from 'react';
import NavBarWithDrawer from './components/navBarWithDrawer/navBarWithDrawer';

const AppRoute = lazy(() => import('./components/AppRoutes'))

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <NavBarWithDrawer />
        <Suspense fallback={<h2>Loading...</h2>}>
          <Routes>
            <Route path='*' element={<AppRoute />} />
          </Routes>
        </Suspense>
      </BrowserRouter>
    </div>
  );
}

export default App;
