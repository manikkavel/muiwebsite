import * as React from 'react';
import ListItems from '../list/sharedWtihMe'
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import SearchIcon from '@mui/icons-material/Search';
import MoreIcon from '@mui/icons-material/MoreVert';
import { Grid } from '@mui/material';
import { Container } from '@mui/system';
import PhotoLibraryIcon from '@mui/icons-material/PhotoLibrary';
import PeopleIcon from '@mui/icons-material/People';
import StarIcon from '@mui/icons-material/Star';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';

const drawerWidth = 240;



const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

export default function PersistentDrawerLeft() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();

  const appBarColor = createTheme({
    palette: {
      primary: {
        main: "#6002ee"
      }
    }
  });

  const sideBarTop = [{
    text: "All Photos",
    icon: <PhotoLibraryIcon />
  },
  {
    text: "Shared with me",
    icon: <PeopleIcon />
  },
  {
    text: "Starred",
    icon: <StarIcon />
  },
  {
    text: "Recent",
    icon: <AccessTimeIcon />
  }]

  const sideBarBottom = [{
    text: "Photoshoots",
    icon: <BookmarkIcon />
  },
  {
    text: "Baseball",
    icon: <BookmarkIcon />
  },
  {
    text: "Jazz events",
    icon: <BookmarkIcon />
  },
  {
    text: "New Year's Eve 2018",
    icon: <BookmarkIcon />
  }]


  const handleSideBarTopClick = (object, index) => {
    if (object.text === "All Photos") { navigate('/') }
    if (object.text === "Shared with me") { navigate('/SharedWithMe') }
    if (object.text === "Starred") { navigate('/Starred') }
    if (object.text === "Recent") { navigate('/Recent') }
  }

  const handleSideBarBottomClick = (object, index) => {
    if (object.text === "Photoshoots") { navigate('/Photoshoots') }
    if (object.text === "Baseball") { navigate('/Baseball') }
    if (object.text === "Jazz events") { navigate('/JazzEvents') }
    if (object.text === "New Year's Eve 2018") { navigate('/NewYearsEve') }
  }

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />

      <AppBar position="fixed" open={open} theme={appBarColor}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Grid variant="h6" noWrap component="div">
            Photos
          </Grid>
          <Grid container justifyContent='flex-end' >
            <IconButton size="large" aria-label="search" color="inherit">
              <SearchIcon className='searchIcon' />
            </IconButton>
            <IconButton
              size="large"
              aria-label="display more actions"
              edge="end"
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Grid>
        </Toolbar>
      </AppBar>

      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          {sideBarTop.map((object, index) => (
            <ListItem key={index} disablePadding>
              <ListItemButton onClick={() => handleSideBarTopClick(object, index)}>
                <ListItemIcon>
                  {object.icon}
                </ListItemIcon>
                <ListItemText primary={object.text} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
        <Typography variant='subtitle1' align='left'>Collections</Typography>
        <Divider />
        <List>
          {sideBarBottom.map((object, index) => (
            <ListItem key={index} disablePadding>
              <ListItemButton onClick={() => handleSideBarBottomClick(object, index)}>
                <ListItemIcon>
                  {object.icon}
                </ListItemIcon>
                <ListItemText primary={object.text} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </Drawer>
      <Main open={open}>
        <DrawerHeader />
      </Main>

    </Box>
  );
}