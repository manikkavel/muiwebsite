import * as React from 'react';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import ButtonBase from '@mui/material/ButtonBase';
import Box from '@mui/material/Box';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ShareIcon from '@mui/icons-material/Share';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { spacing } from '@mui/system';

const Img = styled('img')({
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
});
const cardItems = [{
    img: 'https://cdn.pixabay.com/photo/2016/03/23/04/44/fruits-and-vegetables-1274079_960_720.png',
    heading: 'Kiwis',
    text: 'Collection of high resolution fruit photoshoots for upcoming smoothie receipe cookbook in August.',
    date: 'May 8'
},
{
    img: 'https://images.unsplash.com/photo-1587496679742-bad502958fbf?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80',
    heading: 'Lemons',
    text: 'Collection of high resolution fruit photoshoots for upcoming smoothie receipe cookbook in August. Final edits of lemons in this folder.',
    date: 'May 8'
},
{
    img: 'https://media.istockphoto.com/id/890189900/photo/three-strawberries-with-strawberry-leaf.jpg?s=612x612&w=is&k=20&c=KCBI2c3aoJGD2KXNhwnVELWVeH1MjLOJAJS9ntM4Hhg=',
    heading: 'Strawberries',
    text: 'Collection of high resolution fruit photoshoots for upcoming smoothie receipe cookbook in August.',
    date: 'May 8'
},
{
    img: 'https://media.istockphoto.com/id/152123469/photo/heap-of-tasty-organic-figs-at-local-farmers-market.jpg?s=1024x1024&w=is&k=20&c=1w6q-qGG--UVZ-KaTRDSCff3c6DHa9Q3RAXwNLq2QoU=',
    heading: 'Figs',
    text: 'Second photoshoots of figs moved here. Use this folder for final edits',
    date: 'May 8'
},
{
    img: 'https://media.istockphoto.com/id/1162495264/photo/nectarine-with-green-leaf-and-slices-isolated-on-white-background.jpg?s=612x612&w=0&k=20&c=FUyQPfzQqucFJ3O9huid8TOhcaJ4lP7UeFk9e3K33XY=',
    heading: 'Nectarines',
    text: 'Collection of high resolution fruit photoshoots for upcoming smoothie receipe cookbook in August. Final edits of lemons in this folder.',
    date: 'May 7'
},
{
    img: 'https://images.unsplash.com/photo-1563114773-84221bd62daa?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8d2F0ZXJtZWxvbnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60',
    heading: 'Watermelons',
    text: 'Collection of high resolution fruit photoshoots for upcoming smoothie receipe cookbook in August. Final edits of lemons in this folder.',
    date: 'May 7'
},
]
export default function AllPhotos() {


    return (
        <>
            <Box sx={{justifyContent: 'right', display: 'flex'}} >
                <Typography sx={{mr: 2}}><ShareIcon /></Typography>
                <Typography><FavoriteIcon /></Typography>
            </Box>
            <Paper
                sx={{
                    p: 2,
                    margin: 'auto',
                    maxWidth: 800,
                    flexGrow: 1,
                    backgroundColor: (theme) =>
                        theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
                }}
            >
                {cardItems.map((object) => {
                    return (
                        <Grid container spacing={2}>
                            <Grid item>
                                <ButtonBase sx={{ width: 128, height: 128 }}>
                                    <Img alt="complex" src={object.img} />
                                </ButtonBase>
                            </Grid>
                            <Grid item xs={12} sm container>
                                <Grid rid item xs container direction="column" spacing={2} padding={3}>
                                    <Grid item xs>
                                        <Typography gutterBottom variant="h5" component="div" align='left'>
                                            {object.heading}
                                        </Typography>
                                        <Typography variant="body2" gutterBottom align='left'>
                                            {object.text}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid item >
                                    <Typography variant="subtitle1" component="div" marginLeft={7}>
                                        {object.date}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    )
                })}
            </Paper>
        </>
    );
}