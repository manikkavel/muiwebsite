import React, { lazy } from 'react';
import { Routes, Route } from 'react-router-dom';


const AllPhotos = lazy(() => import('./list/allPhotos'));
const SharedWithMe = lazy(() => import('./list/sharedWtihMe'));
const Starred = lazy(() => import('./list/starred'));
const Recent = lazy(() => import('./list/recent'));
const PhotoShoots = lazy(() => import('./list/photoShoots'));
const Baseball = lazy(() => import('./list/baseBall'));
const JazzEvents = lazy(() => import('./list/jazzEvents'));
const NewYearsEve = lazy(() => import('./list/newYearsEve2018'));

function AppRoutes() {

    return (
        <>
            
            <Routes>
                <Route path='/' element={<AllPhotos />} />
                <Route path='/SharedWithMe' element={<SharedWithMe />} />
                <Route path='/Starred' element={<Starred />} />
                <Route path='/Recent' element={<Recent />} />
                <Route path='/PhotoShoots' element={<PhotoShoots />} />
                <Route path='/Baseball' element={<Baseball />} />
                <Route path='/JazzEvents' element={<JazzEvents />} />
                <Route path='/NewYearsEve' element={<NewYearsEve />} />
            </Routes>
        </>
    );
}
export default AppRoutes;